from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(blank = True)
    description = models.TextField(null=True)
    created_on = models.DateTimeField(auto_now_add=True)
