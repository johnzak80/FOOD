from django.contrib import admin
from recipes.models import Recipe

admin.site.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
    ]
