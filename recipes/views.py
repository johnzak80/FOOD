from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

def create_recipe(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("recipe_list")
    else:
        # Create an instance of the Django model form class
        form = RecipeForm()

    # Put the form in the context
    context = {
      "form": form,
    }
    # Render the HTML template with the form
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method =="POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
        context = {
            "recipe_object": recipe,
            "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)

def show_recipe(request, id):
    #get data from database
    recipe = get_object_or_404(Recipe, id=id)
    #put data in context
    context = {
        "recipe_object": recipe,
    }
    #render HTML with data in contexts
    return render(request, "recipes/detail.html", context)
   

#get data, put that data in a data structure render HTML with that data
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

